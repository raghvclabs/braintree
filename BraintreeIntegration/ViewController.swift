//
//  ViewController.swift
//  BraintreeIntegration
//
//  Created by Click Labs on 3/20/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController, BTDropInViewControllerDelegate {
  let manager: AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
  let serverBase = "https://braintree-sample-merchant.herokuapp.com" // ur app backend server
  var braintree: Braintree?
  
  @IBOutlet var payButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    getToken()
  }
  
  // action button to open view controller of braintree
  @IBAction func payClick(sender: AnyObject) {
    var dropInViewController: BTDropInViewController = braintree!.dropInViewControllerWithDelegate(self)
    dropInViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: Selector("userDidCancel"))
    //Customize the UI
    dropInViewController.summaryTitle = "Tekken 3"
    dropInViewController.summaryDescription = "Game for Generation"
    dropInViewController.displayAmount = "$100"
    var navigationController: UINavigationController = UINavigationController(rootViewController: dropInViewController)
    self.presentViewController(navigationController, animated: true, completion: nil)
  }
  
  // func for find token from the client server
  func getToken() {
    //test client token
    var clientToken = "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI3ZjljMzc4NDZmY2EyNTY2OGM0MGJiZGUzMDY4MTk2ODZkMmQ0NmM5MDE5Y2ZlOWEzOTAxYTRlZmFmYjQyMDM4fGNyZWF0ZWRfYXQ9MjAxNS0wMy0yMFQwOTo1ODo0MC40MDY2ODY5MzMrMDAwMFx1MDAyNm1lcmNoYW50X2lkPWRjcHNweTJicndkanIzcW5cdTAwMjZwdWJsaWNfa2V5PTl3d3J6cWszdnIzdDRuYzgiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZGNwc3B5MmJyd2RqcjNxbi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzL2RjcHNweTJicndkanIzcW4vY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIn0sInRocmVlRFNlY3VyZUVuYWJsZWQiOnRydWUsInRocmVlRFNlY3VyZSI6eyJsb29rdXBVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZGNwc3B5MmJyd2RqcjNxbi90aHJlZV9kX3NlY3VyZS9sb29rdXAifSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQiLCJtZXJjaGFudEFjY291bnRJZCI6InN0Y2gybmZkZndzenl0dzUiLCJjdXJyZW5jeUlzb0NvZGUiOiJVU0QifSwiY29pbmJhc2VFbmFibGVkIjp0cnVlLCJjb2luYmFzZSI6eyJjbGllbnRJZCI6IjExZDI3MjI5YmE1OGI1NmQ3ZTNjMDFhMDUyN2Y0ZDViNDQ2ZDRmNjg0ODE3Y2I2MjNkMjU1YjU3M2FkZGM1OWIiLCJtZXJjaGFudEFjY291bnQiOiJjb2luYmFzZS1kZXZlbG9wbWVudC1tZXJjaGFudEBnZXRicmFpbnRyZWUuY29tIiwic2NvcGVzIjoiYXV0aG9yaXphdGlvbnM6YnJhaW50cmVlIHVzZXIiLCJyZWRpcmVjdFVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tL2NvaW5iYXNlL29hdXRoL3JlZGlyZWN0LWxhbmRpbmcuaHRtbCJ9LCJtZXJjaGFudElkIjoiZGNwc3B5MmJyd2RqcjNxbiIsInZlbm1vIjoib2ZmbGluZSIsImFwcGxlUGF5Ijp7InN0YXR1cyI6Im1vY2siLCJjb3VudHJ5Q29kZSI6IlVTIiwiY3VycmVuY3lDb2RlIjoiVVNEIiwibWVyY2hhbnRJZGVudGlmaWVyIjoibWVyY2hhbnQuY29tLmJyYWludHJlZXBheW1lbnRzLmRldi1kY29wZWxhbmQiLCJzdXBwb3J0ZWROZXR3b3JrcyI6WyJ2aXNhIiwibWFzdGVyY2FyZCIsImFtZXgiXX19"
    // code for fetching token from client server
    /*
    manager.GET("\(serverBase)/token",
    parameters: nil,
    success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
    var clientToken = responseObject["clientToken"] as String
    self.braintree = Braintree(clientToken: clientToken)
    self.payButton.userInteractionEnabled = true
    }) { (operation: AFHTTPRequestOperation!, error: NSError!) -> Void in
    println(error.description)
    }*/
    self.braintree = Braintree(clientToken: clientToken)
    self.payButton.userInteractionEnabled = true
  }
  
  // func for hit to the client server and generate transaction id
  func postNonce(paymentMethodNonce: String) {
    var parameters = ["payment_method_nonce": paymentMethodNonce]
    manager.POST("\(serverBase)/payment",
      parameters: parameters,
      success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
        println(responseObject)
        var transactionId : String = (responseObject["transaction"] as AnyObject!)["id"] as String
        var alert = UIAlertController(title: "Token", message: "Your token is: " + transactionId, preferredStyle: UIAlertControllerStyle.Alert)
        var defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
      }) { (operation: AFHTTPRequestOperation!, error: NSError!) -> Void in
        println(error.description)
    }
  }
  
  // if user click on cancel button on left bar button
  func userDidCancel() {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  //delegates function
  func dropInViewControllerDidCancel(viewController: BTDropInViewController!) {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func dropInViewControllerWillComplete(viewController: BTDropInViewController!) {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func dropInViewController(viewController: BTDropInViewController!, didSucceedWithPaymentMethod paymentMethod: BTPaymentMethod!) {
    postNonce(paymentMethod.nonce)
    self.dismissViewControllerAnimated(true, completion: nil)
  }
}