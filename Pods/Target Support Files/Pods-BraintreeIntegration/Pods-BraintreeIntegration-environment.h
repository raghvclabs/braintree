
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Braintree
#define COCOAPODS_POD_AVAILABLE_Braintree
#define COCOAPODS_VERSION_MAJOR_Braintree 3
#define COCOAPODS_VERSION_MINOR_Braintree 7
#define COCOAPODS_VERSION_PATCH_Braintree 0

// Braintree/API
#define COCOAPODS_POD_AVAILABLE_Braintree_API
#define COCOAPODS_VERSION_MAJOR_Braintree_API 3
#define COCOAPODS_VERSION_MINOR_Braintree_API 7
#define COCOAPODS_VERSION_PATCH_Braintree_API 0

// Braintree/Drop-In
#define COCOAPODS_POD_AVAILABLE_Braintree_Drop_In
#define COCOAPODS_VERSION_MAJOR_Braintree_Drop_In 3
#define COCOAPODS_VERSION_MINOR_Braintree_Drop_In 7
#define COCOAPODS_VERSION_PATCH_Braintree_Drop_In 0

// Braintree/PayPal
#define COCOAPODS_POD_AVAILABLE_Braintree_PayPal
#define COCOAPODS_VERSION_MAJOR_Braintree_PayPal 3
#define COCOAPODS_VERSION_MINOR_Braintree_PayPal 7
#define COCOAPODS_VERSION_PATCH_Braintree_PayPal 0

// Braintree/Payments
#define COCOAPODS_POD_AVAILABLE_Braintree_Payments
#define COCOAPODS_VERSION_MAJOR_Braintree_Payments 3
#define COCOAPODS_VERSION_MINOR_Braintree_Payments 7
#define COCOAPODS_VERSION_PATCH_Braintree_Payments 0

// Braintree/UI
#define COCOAPODS_POD_AVAILABLE_Braintree_UI
#define COCOAPODS_VERSION_MAJOR_Braintree_UI 3
#define COCOAPODS_VERSION_MINOR_Braintree_UI 7
#define COCOAPODS_VERSION_PATCH_Braintree_UI 0

// Braintree/Venmo
#define COCOAPODS_POD_AVAILABLE_Braintree_Venmo
#define COCOAPODS_VERSION_MAJOR_Braintree_Venmo 3
#define COCOAPODS_VERSION_MINOR_Braintree_Venmo 7
#define COCOAPODS_VERSION_PATCH_Braintree_Venmo 0

